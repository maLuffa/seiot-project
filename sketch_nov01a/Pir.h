#ifndef __PIR__
#define __PIR__
#include "Arduino.h"
//l'interfaccia di un sensore pir
class Pir {
public:
  //controlla se qualcosa è stato trovato
  virtual boolean detected() = 0;
  //inizializza il sensore
  virtual void init() = 0;
};
#endif
