#include "LedImpl.h"
#include "GateImpl.h"
#include "Scheduler.h"
#include "Shared.h"
#include "PirImpl.h"
#include "macro.h"
#include "Sonar.h"
#include "ButtonImpl.h"
#include "LedManager.h"
#include "TaskFactory.h"
#include "SharedSensor.h"

//inizializza eventuali sensori del sistema
void setupDevice() {
  SharedSensor::instance()->getPir()->init();
}
//inizializza le variabili condivise
void setupShared() {
  Shared::instance()->carParked();
  Shared::instance()->toCloseGate();
  
}
//inizializza i task e lo scheduler
void setupScheduler(){
  /* */
  Scheduler::instance()->addTask(TaskFactory::instance()->getTaskGate());
  Scheduler::instance()->addTask(TaskFactory::instance()->getTaskSerial());
  Scheduler::instance()->addTask(TaskFactory::instance()->getTaskDetect());
  Scheduler::instance()->addTask(TaskFactory::instance()->getTaskAssistance());
  Scheduler::instance()->init();
}
void setup() {
  Serial.begin(9600);
  Serial.println("___SETUP BEGIN___");
  setupDevice();
  setupShared();
  setupScheduler();
  Serial.println();
  Serial.println("___SETUP END___");
}
void loop() {
  Scheduler::instance()->schedule();
}
