#ifndef __DISTANCE_MANAGER__
#define __DISTANCE_MANAGER__
//utilizzato per gestire la distanza della macchina dal muro
class DistanceManager {
public:
  virtual void actualDistance(float distance) = 0;
};
#endif
