#ifndef __GATE__
#define __GATE__

#include "Arduino.h"
//l'interfaccia che modularizza un cancello
class Gate {
public:
  //dice se il cancello è aperto
  virtual boolean isOpened() = 0;
  //dice se il cancello è chiuso
  virtual boolean isClosed() = 0;
  //apre il cancello gradualmente
  virtual void stepOpen() = 0;
  //chiude il cancello gradualmente
  virtual void stepClose() = 0;
};
#endif
