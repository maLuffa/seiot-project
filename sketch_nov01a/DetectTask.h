#ifndef _DETECT_TASK_
#define _DETECT_TASK_
#include "TaskWithState.h"
#include "Gate.h"
#include "State.h"
#include "Pir.h"
#include "Button.h"
//il task utilizzato per controllare l'arrivo della macchina e la gestione di essa
class DetectTask : public TaskWithState {
private:
public:
  DetectTask(int period);
};
#endif

