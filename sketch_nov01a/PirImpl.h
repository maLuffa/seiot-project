#ifndef _PIR_IMPL_
#define _PIR_IMPL_
#include "Pir.h"
#include "Arduino.h"
class PirImpl : public Pir {
private:
  int pin;
  boolean called;
public:
  PirImpl(int pin);
  //see if something is detected
  virtual boolean detected() ;
  //initialize the sensor
  virtual void init();
};
#endif
