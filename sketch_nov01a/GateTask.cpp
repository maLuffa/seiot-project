#include "BasicState.h"
#include "Shared.h"
#include "SharedSensor.h"
#include "Arduino.h"
#include "GateTask.h"
//la definizioni dei possibili stati di un task
class ClosedState: public  BasicState { //lo stato nel quale il cancello è chiuso
public:
  ClosedState() : BasicState(){}
  void make();
};
class OpenedState: public  BasicState { //lo stato nel quale il cancello è aperto
public:
  OpenedState() : BasicState(){}
  void make();
};
class OpeningState: public BasicState { //lo stato nel quale il cancello si sta aprendo
public:
  OpeningState() : BasicState(){}
  void make();
};
class ClosingState: public BasicState { //lo stato nel quale il cancello si sta chiudendo
public:
  ClosingState() :  BasicState(){}
  void make();
};

//definition of metod make
void ClosedState::make() {
  if(Shared::instance()->getGateState()) {
    _nextState = new OpeningState();
  } else {
    _nextState = this;
  }
}
void OpenedState::make() {
  if(!Shared::instance()->getGateState()) {
    _nextState = new ClosingState();
  } else {
    _nextState = this;
  }
}

void ClosingState::make() {
  if(SharedSensor::instance()->getGate()->isOpened()) {
    SharedSensor::instance()->getGate()->stepClose();
    _nextState = this;
  } else {
    _nextState = new ClosedState();
  }
}

void OpeningState::make() {
  if(SharedSensor::instance()->getGate()->isClosed()) {
    SharedSensor::instance()->getGate()->stepOpen();
    _nextState = this;
  } else {
    _nextState = new OpenedState();
  }
}
//gate task definition  
GateTask::GateTask(int period) : TaskWithState(period){
  this->state = new ClosedState();
}


