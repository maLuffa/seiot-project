#ifndef __TASK_WITH_STATE__
#define __TASK_WITH_STATE__
#include "Task.h"
#include "State.h"
#include "Arduino.h"
//un task con una macchina a stati più o meno complessa
class TaskWithState : public Task {
protected:
  State * state;
public:
  TaskWithState(int period): Task(period){}
  /*in questo caso le nostre macchine a stati funzioneranno cosi(una macchina di harel):
   * quando viene chiamato make sul suo stato vengono eseguite delle operazioni in modo periodico
   * durante make si controllano tutti i vari eventi, se almeno uno di questi è vero si cambierà lo stato successivo della
   * macchina a stati
  */
  virtual void tick() {
    state->make();
    State * next = state->nextState();
    if(next != state) {
      free(state);
      state = next;
    }
  }
};
#endif
