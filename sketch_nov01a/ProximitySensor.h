#ifndef __PROXIMITYSENSOR__
#define __PROXIMITYSENSOR__
//un generico sensore di prossimità
class ProximitySensor {

public:
  virtual float getDistance() = 0;
  
};


#endif

