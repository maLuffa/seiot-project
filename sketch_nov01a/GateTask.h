#ifndef _GATE_TASK_
#define _GATE_TASK_
#include "TaskWithState.h"
#include "Gate.h"
#include "State.h"
//un task che descrive il comportamento di un cancello in movimento
class GateTask : public TaskWithState {
private:
  Gate * gate;
public:
  GateTask(int period);
};
#endif

