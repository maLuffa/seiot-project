#ifndef _GATE_IMPL_
#define _GATE_IMPL_
#include "Gate.h"
#include "ProgressiveLed.h"
class GateImpl : public Gate{
private:
  ProgressiveLed * gate;
  boolean state;
  short currentOpening; 
  short currentStep;
  //we used short because we suppose that the max step to open are 255
  short maxStep;
  short getIntensity();
public:
  GateImpl(ProgressiveLed * led, short stepToMaxOpening);
  //tell if the gate is opened
  virtual boolean isOpened();
  //tell if the gate is closed
  virtual boolean isClosed();
  //open the gate gradually
  virtual void stepOpen();
  //close the gate gradually
  virtual void stepClose();
};

#endif
