//un posto dove condividere tutte le macro, si è preferito usarlo perchè le variabili globali comunque consumano memoria
#define GATE_PIN 3

#define PIR_PIN 2

#define PROX_TRIG 8

#define PROX_ECHO 7

#define TOUCH_PIN 12

#define CLOSE_PIN 13

#define L1_PIN 5

#define L2_PIN 6

#define TIME_TO_OPEN_GATE 2000

#define GATE_PERIOD 100

#define SERIAL_INPUT_PERIOD 200

#define DETECT_PERIOD 200

#define ASSISTANCE_TASK_PERIOD 100

#define STEP_GATE TIME_TO_OPEN_GATE / GATE_PERIOD

#define TIME_TO_WAIT 3000

#define DIST_MIN 10

#define DIST_MAX 100

#define THRESHOLD 200

#define DIST_CLOSE 50

#define WELCOME "Welcome Home"

#define OK "OK"

#define OK_STOP "OK CAN STOP"

#define TOO_FAR "TOO FAR"

#define TOUCHING "TOUCHING"

#define DISTANCE "DISTANCE: "
