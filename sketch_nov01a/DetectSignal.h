#ifndef __DETECTED__
#define __DETECTED__

//una classe utilizza per leggere dei segnali
//il segnale verrà modificato esternamente dalla classe SerialInputTask
enum ReadSignal{NO_SIGNAL = -1,OPEN = 0,STOP = 1};
class DetectSignal{
private:
  ReadSignal * sig;
  DetectSignal();
  static DetectSignal * SINGLETON;
public:
  static DetectSignal * instance() {
    if(!DetectSignal::SINGLETON) {
      DetectSignal::SINGLETON = new DetectSignal();
    }
    return DetectSignal::SINGLETON;
  }
  void init(ReadSignal * sig);
  ReadSignal getCurrentSignal(){
    return *(this->sig);
  }
};

#endif

