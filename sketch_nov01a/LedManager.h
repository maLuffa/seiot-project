#ifndef _LED_MANAGER_
#define _LED_MANAGER_
#include "ProgressiveLed.h"
#include "DistanceManager.h"
//una possibile implementazione di distanceManger attraverso l'utilizzo di due led
class LedManager : public DistanceManager {
private:
  int distMin;
  int distMax;
  ProgressiveLed * l1;
  ProgressiveLed * l2;
public:
  LedManager(int distMin,int distMax,ProgressiveLed * l1, ProgressiveLed * l2);
  void actualDistance(float distance);
};
#endif
