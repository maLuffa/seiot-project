#include "GateImpl.h"
GateImpl::GateImpl(ProgressiveLed * led, short stepToMaxOpening) {
  this->gate = led;
  this->state = false;
  this->currentStep = 0;
  this->maxStep = stepToMaxOpening;
}

boolean GateImpl::isOpened() {
  return this->state;
}

boolean GateImpl::isClosed() {
  return !this->state;
}

void GateImpl::stepOpen() {
  if(!this->isOpened()) {
    this->currentStep ++;
    if(this->currentStep == maxStep) {
      this->state = true;
    }
    gate->setIntensity(this->getIntensity());
  }
}
void GateImpl::stepClose() {
  if(!this->isClosed()) {
    this->currentStep --;
    if(this->currentStep == 0) {
      this->state = false;
    }
    gate->setIntensity(this->getIntensity());
  }
}
short GateImpl::getIntensity(){
  return MAX_LIGHT * (double(currentStep) / double(maxStep));
}
