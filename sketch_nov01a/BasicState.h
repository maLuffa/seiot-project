#ifndef __BASICSTATE__
#define __BASICSTATE__
/* una classe stato base che descrive il comportamente di uno stato in generale
 * su make verrano eseguite le operazioni ed in più si controlleranno gli eventi possibili all'interno di uno stato
 * quindi in questo metodo si setterà il prossimo stato della macchina a stati
 */
#include "State.h"
class BasicState: public State {
protected:
  State * _nextState;
public:
  BasicState() {
    this->_nextState = this;
  }
  State * nextState(){
    return _nextState;
  }
};

#endif
