#ifndef _SERIAL_TASK_
#define _SERIAL_TASK_
#include "DetectSignal.h"
#include "Arduino.h"
/*
 * questo task per quanto semplice è essenziale,
 * se ogni task leggesse da seriale, il task successivo perderebbe il messaggio
 * in questo modo, dando un periodo con il minimo comune multiplo tra tutti i task
 * il messaggio non verrà perso ma rimarrà salvato per tutto il periodo di
 * serialInputTask
 * per poi valutare l'input successivo inviato dall'utente
 */
class SerialInputTask : public Task {
private:
  ReadSignal sig;
public:
  SerialInputTask(int period) :  Task(period){
    sig = NO_SIGNAL;
    DetectSignal::instance()->init(&sig);
  }
  virtual void tick() {
    if (Serial.available() > 0) {
      sig = Serial.read() -'0';
    } else {
      sig = NO_SIGNAL;
    }
  }
};
#endif

