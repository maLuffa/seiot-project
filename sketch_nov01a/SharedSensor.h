#ifndef __S__
#define __S__
#include "LedImpl.h"
#include "GateImpl.h"
#include "PirImpl.h"
#include "macro.h"
#include "Sonar.h"
#include "ButtonImpl.h"
#include "LedManager.h"
/**
 * I sensori sono condivisi per evitare la duplicazione di puntatori in giro per oggetti
 * fatto per questioni di memoria
 */
class SharedSensor {
private:
    Gate * gate;
    Pir * pir;
    ProximitySensor * prox;
    Button * close;
    Button * touch;
    DistanceManager * manager;
    static SharedSensor * S;
    SharedSensor();
public:
    static SharedSensor * instance() {
        if(SharedSensor::S == 0) {
            SharedSensor::S = new SharedSensor();
        }
        return SharedSensor::S;
    }
    Gate * getGate() {
        return this->gate;
    }

    Pir * getPir() {
        return this->pir;
    }
    ProximitySensor * getProx(){
        return this->prox;
    }
    Button * getClose() {
        return this->close;
    }
    Button * getTouch() {
        return this->touch;
    }
    DistanceManager * getManager() {
        return this->manager;
    }
};

#endif
