
#ifndef __SHARED__
#define __SHARED__
//classe che permette l'accesso ai concetti condivisi
class Shared {
private:  
  bool gateState;
  bool entering;
  static Shared* SINGLETON;
  Shared();
public:
  static Shared* instance();
  void toOpenGate(); //permette di aprire il cancello
  void toCloseGate(); //permette di chiudere il cancello
  bool getGateState(); //restituisce true se il cancello è aperto false altrimenti
  void carEntering(); //la macchina sta entrando nel garage
  void carParked();//la macchina ha parcheggiato
  bool getCarState(); //ci dice se la macchina ha parcheggiato o meno
};
#endif

