#include "Shared.h"

Shared* Shared::SINGLETON = 0;

Shared* Shared::instance() {
  if(!Shared::SINGLETON){
    Shared::SINGLETON = new Shared();
  }
  return Shared::SINGLETON;
}

Shared::Shared() {
  this->gateState = false;
  this->entering = false;
}
void Shared :: toOpenGate(){
  this->gateState= true;
}

void Shared :: toCloseGate() {
  this->gateState = false;
}

bool Shared :: getGateState() {
  return this->gateState;
}

void Shared :: carEntering(){
  this->entering = true;
}

void Shared :: carParked() {
  this->entering = false;
}

bool Shared :: getCarState() {
  return this->entering;
}


