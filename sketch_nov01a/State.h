#ifndef __STATE__
#define __STATE__
//l'interfaccia di uno stato generico in questo contesto
class State {
public:
  virtual void make() = 0;
  virtual State * nextState() = 0;
};
#endif
