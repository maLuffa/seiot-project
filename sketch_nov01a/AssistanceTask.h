#ifndef _ASSISTANCE_TASK_
#define _ASSISTANCE_TASK_
#include "Task.h"
#include "Gate.h"
#include "State.h"
#include "Pir.h"
#include "TaskWithState.h"
//il task che gestisce l'assistenza al parcheggio
class AssistanceTask : public TaskWithState {
private:
public:
  AssistanceTask(int period);
};
#endif
