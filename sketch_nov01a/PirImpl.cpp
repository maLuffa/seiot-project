#include "PirImpl.h"
#define CAL_TIME 5
PirImpl::PirImpl(int pin) {
  pinMode(pin,INPUT);
  this->pin = pin;
  this->called = false;
}

void PirImpl::init() {
  if(!called) {
    Serial.println("PIR::");
    for(int i = 0; i < CAL_TIME; i++){
      Serial.print(".");
      delay(1000);
    }
    called = true;
  }
}

boolean PirImpl::detected() {
  if(!called) return false;
  return  digitalRead(this->pin);
}

