#include "DetectSignal.h"

DetectSignal::DetectSignal() {
  this->sig = 0;
}

void DetectSignal::init(ReadSignal * sig) {
  if(!this->sig) {
    this->sig = sig;
  }
}
DetectSignal * DetectSignal::SINGLETON = 0;
