#include "LedManager.h"

LedManager::LedManager(int distMin,int distMax,ProgressiveLed * l1, ProgressiveLed * l2){
  this->distMin = distMin;
  this->distMax = distMax;
  this->l1 = l1;
  this->l2 = l2;
  this->l1->setIntensity(0);
  this->l2->setIntensity(0);
}

void LedManager::actualDistance(float distance) {
  if(distance > distMax) {
    this->l1->setIntensity(0);
    this->l2->setIntensity(0);
  } else if (distance < distMin) {
    this->l1->setIntensity(MAX_LIGHT);
    this->l2->setIntensity(MAX_LIGHT);
  } else {
    int average = ((this->distMax - this->distMin) / 2) + this->distMin;
    int value = distance - average;
    if(value <= 0) {
      this->l1->setIntensity(MAX_LIGHT);
      this->l2->setIntensity(map(distance,average + distMin, distMin, 0 , MAX_LIGHT));
    } else {
      this->l1->setIntensity(map(value,average, 0, 0 , MAX_LIGHT));
      this->l2->setIntensity(0);
    }
  }
}

