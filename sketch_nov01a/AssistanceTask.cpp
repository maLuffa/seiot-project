#include "Button.h"
#include "ProximitySensor.h"
#include "Gate.h"
#include "DistanceManager.h"
#include "BasicState.h"
#include "AssistanceTask.h"
#include "SharedSensor.h"
#include "Shared.h"
#include "Arduino.h"
#include "macro.h"
#include "DetectSignal.h"
class WaitingCarState : public BasicState { //lo stato dove viene controllato se la macchina è arrivata o meno
  public :
    virtual void make();
};

class CheckDistanceState: public BasicState { //lo stato deve controllare la distanza 
private:
public:
  CheckDistanceState() : BasicState() {}
  void make();
};
class StoppingState: public  BasicState { //lo stato dove la macchina si può fermare
public:
  StoppingState() :  BasicState(){}
  void make();
};

class StoppedCarState : public BasicState { //lo stato dove aspetto l'arrivo della macchina
  private:
  public :
    StoppedCarState () : BasicState() {}
    void make();
};

void WaitingCarState::make() {
  if(Shared::instance()->getCarState()) {
    this->_nextState = new CheckDistanceState();
  } else {
    this->_nextState = this;
  }
}

void CheckDistanceState::make() {
  int distance = SharedSensor::instance()->getProx()->getDistance();
  SharedSensor::instance()->getManager()->actualDistance(distance);
  Serial.print(DISTANCE);
  Serial.println(distance);
  if(distance < DIST_MIN) {
    Serial.println(OK_STOP);
    _nextState = new StoppingState();
  } else if(distance < DIST_CLOSE && SharedSensor::instance()->getClose()->isPressed()) {
    _nextState = new StoppedCarState();
    Shared::instance()->toCloseGate();
    Serial.println(OK);
  } else if (DetectSignal::instance()->getCurrentSignal() == STOP) {
     if(distance < DIST_CLOSE) {
        _nextState = new StoppedCarState();
        Shared::instance()->toCloseGate();
        Serial.println(OK);
     } else {
        Serial.println(TOO_FAR);
     }
  } else {
    _nextState = this;
  }
}

void StoppingState::make() {
  int distance = SharedSensor::instance()->getProx()->getDistance();
  if(distance > DIST_MIN && distance < DIST_MAX + THRESHOLD) {
    _nextState = new CheckDistanceState();
  } else if(SharedSensor::instance()->getClose()->isPressed()){
    _nextState = new StoppedCarState();
    Shared::instance()->toCloseGate();
  } else if(distance < DIST_CLOSE && DetectSignal::instance()->getCurrentSignal() == STOP) {
    _nextState = new StoppedCarState();
    Shared::instance()->toCloseGate();
    Serial.println(OK);
  } else {
    _nextState = this;
  }
  if(SharedSensor::instance()->getTouch()->isPressed()) {
    Serial.println(TOUCHING);
  }
}
void StoppedCarState::make() {
  if(SharedSensor::instance()->getGate()->isClosed()) {
    Shared::instance()->carParked();
    _nextState = new WaitingCarState();
    SharedSensor::instance()->getManager()->actualDistance(DIST_MAX + THRESHOLD);
  } else {
    _nextState = this;
  }
}
AssistanceTask::AssistanceTask(int period) : TaskWithState(period) {
  state = new WaitingCarState();
}



