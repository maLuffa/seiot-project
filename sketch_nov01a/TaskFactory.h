#ifndef _TASK_FACTORY_
#define _TASK_FACTORY_
#include "Task.h"
#include "GateTask.h"
#include "SerialInputTask.h"
#include "DetectTask.h"
#include "AssistanceTask.h"
#include "macro.h" 
//una factory per creare tutti i task
class TaskFactory {
private:
  TaskFactory(){}
  static TaskFactory * SINGLETON;
public:
  static TaskFactory * instance(){
    if(TaskFactory::SINGLETON == 0) {
      TaskFactory::SINGLETON = new TaskFactory();
    }
    return TaskFactory::SINGLETON;
  }

  Task * getTaskGate() {
    return new GateTask(GATE_PERIOD);
  }
  Task * getTaskDetect() {
    return new DetectTask(DETECT_PERIOD);
  }
  Task * getTaskSerial() {
    return new SerialInputTask(SERIAL_INPUT_PERIOD);
  }
  Task * getTaskAssistance() {
    return new AssistanceTask(ASSISTANCE_TASK_PERIOD);
  }
};

TaskFactory * TaskFactory::SINGLETON = 0;
#endif
