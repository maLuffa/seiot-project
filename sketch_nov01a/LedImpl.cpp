#include "LedImpl.h"
#include "Arduino.h"
LedImpl::LedImpl(short pin) {
  this->pin = pin;
  pinMode(pin,OUTPUT);
}

void LedImpl::switchOn() {
  analogWrite(pin,MAX_LIGHT);
}

void LedImpl::switchOff() {
  analogWrite(pin,MIN_LIGHT);
}

boolean LedImpl::setIntensity(short intensity) {
  if(!intensity < MIN_LIGHT || intensity > MAX_LIGHT) {
    return false;
  }
  analogWrite(pin,intensity);
  return true;
}
