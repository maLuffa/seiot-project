#include "DetectTask.h"
#include "Shared.h"
#include "BasicState.h"
#include "SerialInputTask.h"
#include "SharedSensor.h"
#include "macro.h"
#include "Arduino.h"

//internamente sono implementati gli stati, esternamente non devono essere visibili!
class WaitSignalState : public BasicState { //lo stato dove viene controllato l'arrivo del segnale di apertura
  public :
    virtual void make();
};

class WaitOpenedState: public BasicState { //lo stato dove aspetto che il cancello sia completamente aperto
public:
  WaitOpenedState() : BasicState(){}
  void make();
};
class WaitClosedState: public  BasicState { //lo stato dove aspetto che il cancello sia completamente chiuso
public:
  WaitClosedState() :  BasicState(){}
  void make();
};

class WaitCarState : public BasicState { //lo stato dove aspetto l'arrivo della macchina
  private: 
    int time;
  public :
    WaitCarState() : BasicState() {
      this->time = 0;
    }
    void resetTimer() {
      this->time = 0;
    }
    virtual void make();
};


class CarDetectedState : public BasicState { //lo stato in cui la macchina è stata trovata
  public :
    virtual void make();
};

void WaitSignalState::make() {
  if(DetectSignal::instance()->getCurrentSignal() == OPEN) {
    _nextState = new WaitOpenedState();
    Shared::instance()->toOpenGate();
  } else {
    _nextState = this;
  }
}

void WaitOpenedState::make() { 
  if(SharedSensor::instance()->getGate()->isOpened()) {
    _nextState = new WaitCarState();
    WaitCarState * toCast = _nextState;
    toCast->resetTimer();
  } else {
    _nextState = this;
  }
}

void WaitCarState::make() {
  this->time += DETECT_PERIOD;
  if(this->time >TIME_TO_WAIT || SharedSensor::instance()->getClose()->isPressed()) {
    _nextState = new WaitClosedState();
    Shared::instance()->toCloseGate();
  } else if (SharedSensor::instance()->getPir()->detected()) {
    _nextState = new CarDetectedState();
    Shared::instance()->carEntering();
    Serial.println(WELCOME);
  } else {
    _nextState = this;
  }
}

void CarDetectedState::make() {
  if(!Shared::instance()->getCarState()) {
    _nextState =  new WaitSignalState();
  } else {
    _nextState = this;
  }
}

void WaitClosedState::make() {
  if(SharedSensor::instance()->getGate()->isClosed()) {
    _nextState = new WaitSignalState();
  } else {
    _nextState = this;
  }
}
//COSTRUTTORE
DetectTask::DetectTask(int period):TaskWithState(period) {
  this->state = new WaitSignalState();
}
