#ifndef _LED_IMPL_
#define _LED_IMPL_
#include "ProgressiveLed.h"
#include "Arduino.h"
class LedImpl : public ProgressiveLed {
private:
  short pin;
public:
  LedImpl(short pin);
  virtual boolean setIntensity(short intensity);
  virtual void switchOn();
  virtual void switchOff();   
};
#endif

