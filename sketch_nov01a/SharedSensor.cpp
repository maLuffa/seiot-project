#include "SharedSensor.h"
SharedSensor::SharedSensor(){
      gate = new GateImpl(new LedImpl(GATE_PIN), STEP_GATE);
      pir = new PirImpl(PIR_PIN);
      prox = new Sonar(PROX_ECHO,PROX_TRIG);
      close = new ButtonImpl(CLOSE_PIN);
      touch = new ButtonImpl(TOUCH_PIN);
      manager = new LedManager(DIST_MIN,DIST_MAX, new LedImpl(L1_PIN), new LedImpl(L2_PIN));
}

SharedSensor * SharedSensor::S = 0;
